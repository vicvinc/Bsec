#include <stdio.h> 
#include <linux/unistd.h>
#include <asm/unistd.h>

#define __NR_query_phyaddr 1136

long query_phyaddr(unsigned int cmd,unsigned long arg)
{
    return syscall(__NR_query_phyaddr,cmd,arg);
}


int main(int argc,char **argv)
{
   int a;
   unsigned long b;

   b=(unsigned long)&a;
   
   query_phyaddr(0,b);
   query_phyaddr(1,b);
   query_phyaddr(2,b);
   printf("a vaddr=%lu\n",&a);
   query_phyaddr(3,b);
  
}


