#! /bin/bash
if [ ! -e /etc/kuxlab ]
then
	echo "KUXSE Labeling"
	/usr/local/sbin/setkse > /dev/null 2>&1
	echo "Updating Kernel"
	rpm -Uvh /tmp/kernel-2.6.32-358.kux.x86_64.rpm --force --nodeps > /dev/null 2>&1
	touch /etc/kuxlab
	reboot
fi
