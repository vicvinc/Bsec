/*
 *  KUX security enhance(KSE) header file
 *
 *  This file contains the KSE header files.
 *
 *  Authors:  Zhou Xiong, <zhouxiong@inspur.com>
 *
 *  Copyright (C) 2013,2014 Inspur, inc
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License version 2,
 *	as published by the Free Software Foundation.
 */

#ifdef CONFIG_KUX_SECURITY_ENHANCE

/* Mandatory access controll struct  */
struct mac_level;
/* Intigrety level struct */
struct intigrety_level;
struct task_security_struct;
struct inode_security_struct;
struct user_inode_security_struct; /* just for syscall, no mutex */

extern int kse_enabled;
extern int kse_noyaudit;
extern void kse_cap_audit(struct task_struct *, int, int);
#else
int kse_enabled = 0;
int kse_noyaudit = 1;
void kse_cap_audit(struct task_struct *tsk, int cap, int result)
{
	return;
}
#endif
